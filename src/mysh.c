/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include "parser.h"
#include "utils.h"
#include "fs.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <assert.h>	
#include <errno.h>
#include <fcntl.h>
#include <wait.h>

#include <pthread.h>
#include <dirent.h>
#include <ctype.h>

#include <signal.h>

#include <sys/socket.h>
#include <sys/un.h>
#include <sys/io.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

void mySignalHandler1(void);
void mySignalHandler2(int chpidStorage[]);
void *tfunc_srv(void *argvptr);
void *tfunc_cli(void *argvptr);
void myitoa(int num, char **str);

//return -1: 기호 |가 없음, return +N: 기호 |가 존재.
//제 2 명령어의 시작 Index를 반환, 기호 |의 argv를 NULL로 교체.
int find_pipeChar_argv(int *argc, char ***argv);

//return -1: 기호 &가 없음, return +N: 기호 &가 존재.
//제 2 명령어의 시작 INDEX를 반환, 기호 &의 argv를 NULL로 교체.
int find_bgChar_argv(int *argc, char ***argv);

//파일 /proc/PIDs/stat을 조사, Zombie를 찾아 PID를 반환.
//없으면 0을 반환.
int findZombie();

//파일 디스크립터 관리
int fd[2];

//socket 절대경로
char *socket_path = "/tmp/sockTmp";

int main()
{
  mySignalHandler1();
  char command_buffer[4096] = { 0, };
  int isPipe;
  int isBg;
  pid_t chpid;
  pid_t papid = getpid();
  pid_t bgpid = 0;
  int bgStatus;
  int chpidStorage[8192] = { -1, };
  int chpidIndex = 0;

  while (fgets(command_buffer, 4096, stdin) != NULL) {
    waitpid(bgpid, &bgStatus, WNOHANG);

    if (strcmp(command_buffer, "\n") == 0){
      fprintf(stderr, "Type Commands: ");
      continue;
    }

    isPipe = 0;
    int argc = -1;
    char** argv = NULL;

    parse_command(command_buffer, &argc, &argv);

    assert(argv != NULL);
    if (strcmp(argv[0], "exit") == 0) {
      //#TODO: 
      FREE_2D_ARRAY(argc, argv);
      fprintf(stdout, "#Waiting a moment for children processes be finished...\n");
      waitpid(-1, NULL, 0);
      mySignalHandler2(chpidStorage);
      break;
    }

    struct command_entry* comm_entry = fetch_command(argv[0]);
    if (comm_entry != NULL) {
      //#TODO: My Commands
      if ((isPipe = find_pipeChar_argv(&argc, &argv)) != -1){
        //#TODO: My Commands & Pipe
        pthread_t pth[2];
        pid_t tpid1, tpid2;
        if ((tpid1 = fork()) == 0){
          pthread_create(&pth[0], NULL, tfunc_srv, (void *)(&argv[isPipe]));
          sleep(1);
          if ((tpid2 = fork()) == 0){
            pthread_create(&pth[1], NULL, tfunc_cli, (void *)(&argv[0]));
          }
          else{
              waitpid(tpid2, NULL, 0);
          }
          pthread_join(pth[1], NULL);
        }
        else{
          waitpid(tpid1, NULL, 0);
          chpidStorage[chpidIndex++] = tpid1;
          chpidStorage[chpidIndex++] = tpid2;
        }
        pthread_join(pth[0], NULL);
      }
      else{
        //#TODO: My Commands & No pipe
        if (strcmp(argv[0], "fg") == 0){
          myitoa(bgpid, &argv[1]);
          argc++;
        }

        if ((isBg = find_bgChar_argv(&argc, &argv)) != -1){
          //TODO: My Commands & No pipe & Background Processing
          //Example Code: wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb & ls -all
          if ((bgpid = fork()) == 0){
            setpgid(0, 0);
            execvp(argv[0], argv);
          }
          else if (bgpid > 0){
            waitpid(bgpid, &bgStatus, WNOHANG);
            chpidStorage[chpidIndex++] = bgpid;
          }
          else{
            perror("Background Proc Fork Fail: ");
          }
          if ((chpid = fork()) == 0){
            execvp(argv[isBg], &argv[isBg]);
          }
          else if (chpid > 0){
            waitpid(chpid, NULL, 0);
            chpidStorage[chpidIndex++] = chpid;
          }
          else{
            perror("Fork Fail: ");
          }
        }
        else{
          //TODO: My Commands & No pipe & No Background Processing
          int ret = comm_entry->command_func(argc, argv);
          if (ret != 0) {
            comm_entry->err(ret);          
          }
        }          
      }
    }
    else if (does_exefile_exists(argv[0])) {
      //#TODO: Not My Commands
      if (does_exefile_exists(argv[0]) == -1){
        //#TODO: Not My Commands & Cannot Find Commands, Need goto first of while.
        assert(comm_entry == NULL);
        fprintf(stderr, "%s: command not found.\n", argv[0]);
      }
      else{
        //#TODO, Not My Commands & Can Executable
        if ((isPipe = find_pipeChar_argv(&argc, &argv)) != -1){
          //#TODO, Not My Commands & Can Executable & Pipe
          pthread_t pth[2];
          pid_t tpid1, tpid2;
          if ((tpid1 = fork()) == 0){
            pthread_create(&pth[0], NULL, tfunc_srv, (void *)(&argv[isPipe]));
            sleep(1);
            if ((tpid2 = fork()) == 0){
              pthread_create(&pth[1], NULL, tfunc_cli, (void *)(&argv[0]));
            }
            else{
              waitpid(tpid2, NULL, 0);
            }
            pthread_join(pth[1], NULL);
          }
          else{
            waitpid(tpid1, NULL, 0);
            chpidStorage[chpidIndex++] = tpid1;
            chpidStorage[chpidIndex++] = tpid2;
          }
          pthread_join(pth[0], NULL);
        }
        else{
          //#TODO, Not my commands & Can executable & No pipe
          if ((isBg = find_bgChar_argv(&argc, &argv)) != -1){
            //#TODO, Not my commands & Can executable & No pipe & Background Processing
            //wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb & ls -all

            if ((bgpid = fork()) == 0){
              setpgid(0, 0);
              execvp(argv[0], argv);
            }
            else if (bgpid > 0){
              waitpid(bgpid, &bgStatus, WNOHANG);
              chpidStorage[chpidIndex++] = bgpid;
            }
            else{
              perror("Background process fork fail: ");
            }

            if ((chpid = fork()) == 0){
              execvp(argv[isBg], &argv[isBg]);
            }
            else if (chpid > 0){
              waitpid(chpid, NULL, 0);
              chpidStorage[chpidIndex++] = chpid;
            }
            else{
              perror("Fork Fail: ");
            }
          }
          else{
            //#TODO, Not my commands & Can executable & No pipe & No Background Processing
            if ((chpid = fork()) == 0){
              execvp(argv[0], argv);
            }
            else if (chpid > 0){
              waitpid(chpid, NULL, 0);
              chpidStorage[chpidIndex++] = chpid;
            }
            else{
              perror("Fork Fail: ");
            }
          }
        }
      }
    }
    else {
      //Wrong commands
      assert(comm_entry == NULL);
      fprintf(stderr, "%s: command not found.\n", argv[0]);
    }
    FREE_2D_ARRAY(argc, argv);
  }  
  return 0;
}

void mySignalHandler1(void){
  signal(SIGTTOU, SIG_IGN);
  signal(SIGINT, SIG_IGN);
  signal(SIGTSTP, SIG_IGN);
}

void mySignalHandler2(int chpidStorage[]){
  int pid;
  if ((pid = findZombie()) > 0){
    for (int i = 0; chpidStorage[i] < 0; i++){
      if (chpidStorage[i] == pid){
        fprintf(stdout, "#Zombie Alert!, PID: %d\n", findZombie());
      }
      else{
        continue;
      }
    }
  }
}


void *tfunc_srv(void *argvptr){
    char **argv = (char **)argvptr;
    int srvsd = socket(AF_UNIX, SOCK_STREAM, 0);
    int rc, cl;

    pid_t pidchild;

    struct sockaddr_un srvaddr, cliaddr;
    int clilen = sizeof(cliaddr);
    memset(&srvaddr, 0, sizeof(srvaddr));
    srvaddr.sun_family = AF_UNIX;

    if (*socket_path == '\0') {
      *srvaddr.sun_path = '\0';
      strncpy(srvaddr.sun_path+1, socket_path+1, sizeof(srvaddr.sun_path)-2);
    } 
    else {
      strncpy(srvaddr.sun_path, socket_path, sizeof(srvaddr.sun_path)-1);
      unlink(socket_path);
    }

    int reuse = 1;
    setsockopt(srvsd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

    bind(srvsd, (struct sockaddr*)&srvaddr, sizeof(srvaddr));
    listen(srvsd, 5);

    fd[0] = accept(srvsd, (struct sockaddr*)&cliaddr, &clilen);
    dup2(fd[0], STDIN_FILENO);
    execvp(argv[0], argv);
}
void *tfunc_cli(void *argvptr){
    char **argv = (char **)argvptr;

    fd[1] = socket(AF_UNIX, SOCK_STREAM, 0);

    struct sockaddr_un srvaddr;
    memset(&srvaddr, 0, sizeof(srvaddr));
    srvaddr.sun_family = AF_UNIX;

    if (*socket_path == '\0') {
      *srvaddr.sun_path = '\0';
      strncpy(srvaddr.sun_path+1, socket_path+1, sizeof(srvaddr.sun_path)-2);
    }
    else {
      strncpy(srvaddr.sun_path, socket_path, sizeof(srvaddr.sun_path)-1);
    }
    
    connect(fd[1], (struct sockaddr*)&srvaddr, sizeof(srvaddr));

    dup2(fd[1], STDOUT_FILENO);
    execvp(argv[0], argv);
}

int find_pipeChar_argv(int *argc, char ***argv){
    for (int i = 0; (*argv)[i] != NULL; i++){
        if (strchr((*argv)[i], '|')){
            free((*argv)[i]);
            (*argv)[i] = NULL;
            return i + 1;
        }
    }
    return -1;
}


int find_bgChar_argv(int *argc, char ***argv){
    for (int i = 0; (*argv)[i] != NULL; i++){
        if (strchr((*argv)[i], '&')){
            free((*argv)[i]);
            (*argv)[i] = NULL;
            return i + 1;
        }
    }
    return -1;
}


void myitoa(int num, char **str){ 
    *str = (char *)malloc(sizeof(int) * 128);
    int i = 0; 
    int deg = 1; 
    int cnt = 0; 

    while(1){
      if((num/deg) > 0) {
        cnt++;
      } 
      else{
        break;
      }
      deg *= 10; 
    } 
    deg /= 10;
    for(i = 0; i < cnt; i++) {
      *(*(str) + i) = num / deg + '0';
      num -= ((num / deg) * deg);
      deg /= 10;
    } 
    *((*str) + i) = '\0'; 
}

int findZombie(){
    char path[128];
    DIR *open_proc = opendir("/proc/");
    struct dirent *dirptr;
    struct stat sb;
    for (dirptr = readdir(open_proc); dirptr != NULL; dirptr = readdir(open_proc)){
        char buf[512];
        sprintf(buf, "/proc/%s", dirptr->d_name);
        stat(buf, &sb);
        if (S_ISDIR(sb.st_mode) && isdigit((dirptr->d_name)[0])){
            //경로 /proc에서 디렉터리 이름이 숫자인 것들
            char buf2[512];
            sprintf(buf2, "%s/stat", buf);
            int fd = open(buf2, O_RDONLY);
            char fdbuf[8192];
            read(fd, fdbuf, sizeof(fdbuf));
            //fprintf(stdout, "PATH: %s\n", buf2);

            char *ptr = strtok(fdbuf, " ");
            ptr = strtok(NULL, " ");
            ptr = strtok(NULL, " ");
            //fprintf(stdout, "STAT: %s\n", ptr);
            if (strcmp(ptr, "Z") == 0){
                //fprintf(stdout, "PID: %s is Zombie!\n", dirptr->d_name);
                return atoi(dirptr->d_name);
            }
        }
        else{
            continue;
        }
    }
    closedir(open_proc);
    return 0;
}