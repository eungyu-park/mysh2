/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#include "commands.h"
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>


static struct command_entry commands[] =
{
  {
    "pwd",
    do_pwd,
    err_pwd
  },
  {
    "cd",
    do_cd,
    err_cd
  },
  {
    "kill",
    do_kill,
    err_kill
  },
  {
    "fg",
    do_fg,
    err_fg
  }
};

struct command_entry* fetch_command(const char* command_name)
{
  // TODO: Fill it.
  char *clone = (char *)malloc(strlen(command_name) + 1);
  strcpy(clone, command_name);
  
  char *ptr = strtok(clone, "\n");
  while(ptr != NULL){
    if (!strcmp(clone, commands[0].command_name)){
      free(clone);
      return &(commands[0]);
    }
    else if (!strcmp(clone, commands[1].command_name)){
      free(clone);
      return &(commands[1]);
    }
    else if (!strcmp(clone, commands[2].command_name)){
      free(clone);
      return &(commands[2]);
    }
    else if (!strcmp(clone, commands[3].command_name)){
      free(clone);
      return &(commands[3]);
    }
    ptr = strtok(NULL, "\n");
  }
  free(clone);
  return NULL;
}

int do_pwd(int argc, char** argv)
{
  // TODO: Fill it.
  char pwd_path[1024];
  if (getcwd(pwd_path, 1024) != NULL){
    fprintf(stdout, "%s\n", pwd_path);
    memset(pwd_path, '0', 1024);
    return 0;
  }
  return -1;
}

void err_pwd(int err_code)
{
  // TODO: Fill it.
}

int do_cd(int argc, char** argv)
{
  // TODO: Fill it.
  if (chdir(argv[1]) == -1){
    if (errno == ENOENT){
      return 1;
    }
    else if (errno == ENOTDIR){
      return 2;
    }
  }
  else{
    return 0;
  }
}

void err_cd(int err_code)
{
  // TODO: Fill it.
  if (err_code == 1){
    fprintf(stderr, "cd: no such file or directory\n");
  }
  else if (err_code == 2){
    fprintf(stderr, "cd: not a directory\n");
  }
}

int do_kill(int argc, char** argv){
  if (argc < 2){
    return 1;
  }
  else {
    pid_t target = (pid_t)atoi(argv[1]);
    kill(target, SIGKILL);
    return 0;
  }
}

void err_kill(int err_code){
  if (err_code == 1){
    fprintf(stderr, "kill: arguments error\n");
  }
}

int do_fg(int argc, char **argv){
  pid_t papid = getpid();
  pid_t chpid = atoi(argv[1]);
  pid_t curfg = tcgetpgrp(STDOUT_FILENO);
  tcsetpgrp(STDIN_FILENO, chpid);
  curfg = tcgetpgrp(STDOUT_FILENO);
    
  waitpid(curfg, NULL, 0);
  tcsetpgrp(STDIN_FILENO, papid);
  curfg = 0;
}

//wget https://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.10.tgz & 
//wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb &

void err_fg(int err_code){
  if (err_code == 1){
    fprintf(stderr, "fg: arguments error\n");
  }
}
