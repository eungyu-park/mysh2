/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#define MAX_TOKEN_NUMBER  128

#include "parser.h"
#include <string.h>
#include <stdlib.h>

void rm_newline(char *input){
  for (; *input != '\0'; input++){
    if (*input == '\n'){
      strcpy(input, input + 1);
      input--;
    }
  }
}

void parse_command(const char* input,
                   int* argc, char*** argv)
{
  // TODO: Fill it!
  if (input != NULL){
    *argc = 0;
  }
  else{
    return;
  }

  *argv = (char **)malloc(sizeof(char *) * MAX_TOKEN_NUMBER);

  char *clone = (char *)malloc(strlen(input) + 1);
  strcpy(clone, input);
  rm_newline(clone);
  
  if (strchr(clone, '\"')){
    int tmpct = 0;
    char *tmp[MAX_TOKEN_NUMBER];
    char *delim = strtok(clone, "\"");
    while(delim != NULL){
      tmp[tmpct] = delim;
      delim = strtok(NULL, "\"");
      tmpct++;
    }
    
    for (int i = 0; i < tmpct; i++){
      if (i == tmpct - 1){
        (*argv)[*argc] = (char *)malloc(strlen(tmp[i]) + 1);
        strcpy((*argv)[*argc], tmp[i]);
        (*argc)++;
        break;
      }

      char *token = strtok(tmp[i], " ");
      while(token != NULL){
        (*argv)[*argc] = (char *)malloc(strlen(token) + 1);
        strcpy((*argv)[*argc], token);
        token = strtok(NULL, " ");
        (*argc)++;
      }
    }

  }
  else {
    char *token = strtok(clone, " ");
      while(token != NULL){
        (*argv)[*argc] = (char *)malloc(strlen(token) + 1);
        strcpy((*argv)[*argc], token);
        token = strtok(NULL, " ");
        (*argc)++;
      }
  }
  (*argv)[*argc] = NULL;

  //token TEST
  /*
  for (int i = 0; i < *argc; i++){
    printf("(*argv)[%d]: %s\n", i, (*argv)[i]);
  }
  */
  free(clone);
}