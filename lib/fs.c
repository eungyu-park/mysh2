/**********************************************************************
 * Copyright (C) Jaewon Choi <jaewon.james.choi@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 *********************************************************************/
#define  MAX_PATH_NUM     32
#define  MAX_PATH_LENGTH  128
#include "fs.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int resolve_path(const char *path){

    struct stat sample;
    char *envptr = getenv("PATH");
    char *clone = (char *)malloc(strlen(envptr) + 1);
    strcpy(clone, envptr);
    int status;
    char *ptr = strtok(clone, ":");
    char *tmp = (char *)malloc(128);

    while (ptr != NULL){
        sprintf(tmp, "%s/%s", ptr, path);
        if (stat(tmp, &sample) == 0){
            free(clone);
            free(tmp);
            return 1;
        }
        ptr = strtok(NULL, ":");
    }
    free(clone);
    free(tmp);
    return -1;
}

int does_exefile_exists(const char* path)
{
  // TODO: Fill it!
  struct stat sample;
  char buf[1024];
  
  if (stat(path, &sample) == -1){
    if (resolve_path(path) == 1){
      return 1;
    }
    return -1;
  }
  
  if ((sample.st_mode & S_IXUSR) &&
  (sample.st_mode & S_IXGRP) &&
  (sample.st_mode & S_IXOTH)){
    if ((sample.st_mode & __S_IFMT) != __S_IFDIR){
      return 1;
    }
  }
  else {
    return 0;
  }
}
